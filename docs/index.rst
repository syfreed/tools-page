.. Example project documentation master file, created by
   sphinx-quickstart on Mon Mar 22 14:17:44 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Example project's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents

   API <api>
   Greet <api/example_package.greet>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
