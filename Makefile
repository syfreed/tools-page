PACKAGE_NAME ?= example_package
PORT_DOCS ?= 8020
PORT_TEST ?= 8080
REPORTS_DIR ?= reports
SPHINX_SOURCE_DIR ?= docs
SPHINX_BUILD_DIR ?= docs/_build
SRC_DIR ?= src

.PHONY: format
format:
	black ${SRC_DIR}

.PHONY: test
test:
	mkdir -p ${REPORTS_DIR}
	tox -- --source=${PACKAGE_NAME} > ${REPORTS_DIR}/test.txt
	coverage html -d ${REPORTS_DIR}/coverage
	coverage report
.PHONY: testserve
testserve:
	python -m http.server ${PORT_TEST} -d ${REPORTS_DIR}/coverage

.PHONY: lint
lint:
	mkdir -p ${REPORTS_DIR}
	pylint ${SRC_DIR} --reports=y > ${REPORTS_DIR}/lint.txt

.PHONY: typehint
typehint:
	mkdir -p ${REPORTS_DIR}
	mypy ${SRC_DIR} > ${REPORTS_DIR}/typehint.txt

.PHONY: docmake
docmake:
	python setup.py build_sphinx\
		--source ${SPHINX_SOURCE_DIR}\
		--build-dir ${SPHINX_BUILD_DIR}
.PHONY: doctest
doctest:
	sphinx-build ${SPHINX_SOURCE_DIR} ${REPORTS_DIR} -b doctest
.PHONY: docserve
docserve:
	python -m http.server ${PORT_DOCS} -d ${SPHINX_BUILD_DIR}/html
.PHONY: docs
docs: docmake doctest

.PHONY: pipeline
pipeline: format test lint typehint docs
